package br.com.casa.ex3.Test;

import br.com.casa.ex3.Cambio;
import br.com.casa.ex3.ConversorDeMoeda;
import org.junit.Assert;
import org.junit.Test;

public class ConversorDeMoedaTest {

    @Test
    public void
            deveConverterDeRealParaDolar() {
                
                
            ConversorDeMoeda conversor = new ConversorDeMoeda();
            double resultado = conversor.calcular(1, 1);
            
            Assert.assertEquals(2.5, resultado, 0.01);

    }

    {

    }

}
